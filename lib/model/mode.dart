import 'package:equatable/equatable.dart';

class ZoomData {
  double? minzoom;
  double? maxzoom;

  ZoomData(
    this.minzoom,
    this.maxzoom,
  );
}

class User extends Equatable {
  final String? token;

  const User({this.token});

  @override
  List<Object?> get props => [token];

  static const empty = User(token: '_');
}
