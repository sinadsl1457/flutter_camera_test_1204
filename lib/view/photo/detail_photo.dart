import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_camera_test_1204/bloc/photo_bloc.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

class PhotoDetailPage extends HookConsumerWidget {
  const PhotoDetailPage(this.imagePath, {super.key});

  final String imagePath;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final imagefile = File(imagePath);
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
            onPressed: () {
              context.read<CameraBloc>().add(onBackButtonEvent());
            },
            icon: Icon(Icons.arrow_back_ios)),
      ),
      body: Image.file(
        imagefile,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        fit: BoxFit.contain,
      ),
    );
  }
}
