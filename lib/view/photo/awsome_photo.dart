import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_camera_test_1204/bloc/photo_bloc.dart' as bloc;
import 'package:flutter_camera_test_1204/model/mode.dart';
import 'package:flutter_camera_test_1204/view/photo/detail_photo.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';
import 'package:camerawesome/camerawesome_plugin.dart';
import 'package:path_provider/path_provider.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:permission_handler/permission_handler.dart';

final resolutionInfoProvider = StateProvider<String>((ref) {
  return '';
});

class AwesomeCameraPage extends HookConsumerWidget {
  AwesomeCameraPage(
    this.context, {
    super.key,
  });

  final BuildContext context;
  double? resolution;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final saveconfig = SaveConfig.photo(
      pathBuilder: (sensors) async {
        final Directory extDir = await getTemporaryDirectory();
        final testDir = await Directory('${extDir.path}/camerawesome')
            .create(recursive: true);

        if (sensors.length == 1) {
          final String filePath =
              '${testDir.path}/${DateTime.now().millisecondsSinceEpoch}.jpg';
          final request = SingleCaptureRequest(filePath, sensors.first);
          var photostatus = await Permission.photos.request();

          return request;
        } else {
          return MultipleCaptureRequest(
            {
              for (final sensor in sensors)
                sensor:
                    '${testDir.path}/${sensor.position == SensorPosition.front ? 'front_' : "back_"}${DateTime.now().millisecondsSinceEpoch}.jpg',
            },
          );
        }
      },
    );

    return BlocListener<bloc.CameraBloc, bloc.CameraState>(
      listener: (context, state) {
        switch (state.status) {
          case bloc.CameraStatus.ontapMediaCapture:
            final path = state.filepath;
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PhotoDetailPage(path)),
            );
          case bloc.CameraStatus.pop:
            Navigator.pop(context);
          default:
            break;
        }
      },
      child: CameraAwesomeBuilder.awesome(
        saveConfig: saveconfig,
        middleContentBuilder: (state) => MiddleContentsWidget(state),
        topActionsBuilder: (state) => CameraInfoContainer(state),
        bottomActionsBuilder: (state) => AwesomeBottomActions(
            state: state,
            right: ZoomSelector(state),
            captureButton: CustomCaptureButton(state)),
      ),
    );
  }
}

class CustomCaptureButton extends HookConsumerWidget {
  CustomCaptureButton(this.state, {super.key});

  final CameraState state;
  double _scale = 0.0;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final _animatedController = useAnimationController(
      duration: const Duration(milliseconds: 100),
      lowerBound: 0.0,
      upperBound: 0.1,
    );

    _scale = 1 - _animatedController.value;

    if (state is AnalysisController) {
      return Container();
    }
    return GestureDetector(
      onTapDown: (detail) {
        HapticFeedback.selectionClick();
        _animatedController.forward();
      },
      onTapUp: (detail) async {
        Future.delayed(const Duration(milliseconds: 100), () {
          _animatedController.reverse();
        });

        state.when(
          onPhotoMode: (photoState) async {
            final captureRequest = await photoState.takePhoto();
            print(captureRequest);
            captureRequest.when(single: (single) async {
              final bytedata = await single.file?.readAsBytes();

              final resolution =
                  await decodeImageFromList(bytedata!.buffer.asUint8List());

              final result = await ImageGallerySaver.saveImage(
                  bytedata.buffer.asUint8List());
              print(resolution);
              print(result);
              ref.watch(resolutionInfoProvider.notifier).state =
                  '${resolution.width}X${resolution.height}';
            });
          },
          onVideoMode: (videoState) => videoState.startRecording(),
          onVideoRecordingMode: (videoState) => videoState.stopRecording(),
        );
      },
      onTapCancel: () {
        _animatedController.reverse();
      },
      child: SizedBox(
        key: const ValueKey('cameraButton'),
        height: 80,
        width: 80,
        child: Transform.scale(
          scale: _scale,
          child: CustomPaint(
            painter: state.when(
              onPhotoMode: (_) => CameraButtonPainter(),
              onPreparingCamera: (_) => CameraButtonPainter(),
              onVideoMode: (_) => VideoButtonPainter(),
              onVideoRecordingMode: (_) =>
                  VideoButtonPainter(isRecording: true),
            ),
          ),
        ),
      ),
    );
  }
}

class CameraButtonPainter extends CustomPainter {
  CameraButtonPainter();

  @override
  void paint(Canvas canvas, Size size) {
    var bgPainter = Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true;
    var radius = size.width / 2;
    var center = Offset(size.width / 2, size.height / 2);
    bgPainter.color = Colors.white.withOpacity(.5);
    canvas.drawCircle(center, radius, bgPainter);

    bgPainter.color = Colors.white;
    canvas.drawCircle(center, radius - 8, bgPainter);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class VideoButtonPainter extends CustomPainter {
  final bool isRecording;

  VideoButtonPainter({
    this.isRecording = false,
  });

  @override
  void paint(Canvas canvas, Size size) {
    var bgPainter = Paint()
      ..style = PaintingStyle.fill
      ..isAntiAlias = true;
    var radius = size.width / 2;
    var center = Offset(size.width / 2, size.height / 2);
    bgPainter.color = Colors.white.withOpacity(.5);
    canvas.drawCircle(center, radius, bgPainter);

    if (isRecording) {
      bgPainter.color = Colors.red;
      canvas.drawRRect(
          RRect.fromRectAndRadius(
              Rect.fromLTWH(
                17,
                17,
                size.width - (17 * 2),
                size.height - (17 * 2),
              ),
              const Radius.circular(12.0)),
          bgPainter);
    } else {
      bgPainter.color = Colors.red;
      canvas.drawCircle(center, radius - 8, bgPainter);
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}

class MiddleContentsWidget extends HookConsumerWidget {
  const MiddleContentsWidget(this.state, {super.key});
  final CameraState state;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Spacer(),
        Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Align(
                alignment: Alignment.center,
                child: AwesomeZoomSelector(state: state)),
            state is VideoRecordingCameraState
                ? const SizedBox(width: 48)
                : StreamBuilder<MediaCapture?>(
                    stream: state.captureState$,
                    builder: (context, snapshot) {
                      if (!snapshot.hasData) {
                        return const SizedBox(width: 60, height: 60);
                      }
                      return Align(
                        alignment: Alignment.centerRight,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 20),
                          child: SizedBox(
                            width: 60,
                            child: AwesomeMediaPreview(
                              mediaCapture: snapshot.requireData,
                              onMediaTap: (media) {
                                media.captureRequest.when(
                                  single: (single) async {
                                    debugPrint('single: ${single.file?.path}');
                                    single.file?.openRead();

                                    final bytedata =
                                        await single.file?.readAsBytes();

                                    final resolution =
                                        await decodeImageFromList(
                                            bytedata!.buffer.asUint8List());

                                    final result =
                                        await ImageGallerySaver.saveImage(
                                            bytedata.buffer.asUint8List());
                                    print(resolution);
                                    print(result);
                                    ref
                                            .watch(resolutionInfoProvider.notifier)
                                            .state =
                                        '${resolution.height}X${resolution.width}';

                                    context.read<bloc.CameraBloc>().add(
                                        bloc.OnTapMediaCaptureEvent(
                                            single.file?.path ?? ''));
                                  },
                                );
                              },
                            ),
                          ),
                        ),
                      );
                    },
                  ),
          ],
        )
      ],
    );
  }
}

class CameraInfoContainer extends HookConsumerWidget {
  const CameraInfoContainer(this.state, {super.key});
  final CameraState state;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final resolutionInfo = ref.watch(resolutionInfoProvider);
    return Visibility(
      visible: resolutionInfo.isNotEmpty,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
        width: double.infinity,
        height: 150,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12),
        ),
        child: Text('your camera resolution is $resolutionInfo'),
      ),
    );
  }
}

class ZoomSelector extends HookConsumerWidget {
  const ZoomSelector(this.state, {super.key});
  final CameraState state;

  Future<ZoomData> getzoomData() async {
    final maxzoom = await CamerawesomePlugin.getMaxZoom();
    return ZoomData(1.0, maxzoom);
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return FutureBuilder(
      future: getzoomData(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return const SizedBox.shrink();
        }
        final zoomdata = snapshot.data;

        return ZoomPagerMode(zoomdata!, state);
      },
    );
  }
}

final pageindexProvider = StateProvider<int>((ref) {
  return 0;
});

class ZoomPagerMode extends HookConsumerWidget {
  const ZoomPagerMode(this.zoomData, this.cameraState, {super.key});
  final ZoomData zoomData;
  final CameraState cameraState;
  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final pageIndex = ref.watch(pageindexProvider);
    final _pageController = usePageController(viewportFraction: 0.25);

    cameraState.sensorConfig.zoom$.listen(
      (event) {
        print('your zoom: $event');
      },
    );

    return pageview(
      _pageController,
      (index) {
        ref.watch(pageindexProvider.notifier).state = index;
        index == 0
            ? cameraState.sensorConfig.setZoom(0.0)
            : cameraState.sensorConfig.setZoom(1.0);
      },
      pageIndex,
      zoomData,
    );
  }

  Widget pageview(
    PageController _controller,
    void Function(int) onPageChanged,
    int pageindex,
    ZoomData zoomData,
  ) {
    final children = List.generate(
        2,
        (index) => AnimatedOpacity(
              duration: const Duration(milliseconds: 300),
              opacity: index == pageindex ? 1 : 0.2,
              child: AwesomeBouncingWidget(
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8),
                    child: Text(
                      index == 0
                          ? '${zoomData.minzoom}'
                          : '${zoomData.maxzoom}',
                      style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        shadows: [
                          Shadow(
                            blurRadius: 4,
                            color: Colors.black,
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                onTap: () {
                  _controller.animateToPage(
                    index,
                    curve: Curves.easeIn,
                    duration: const Duration(milliseconds: 200),
                  );
                },
              ),
            ));

    return Row(
      children: [
        Expanded(
            child: SizedBox(
          height: 32,
          child: PageView(
            scrollDirection: Axis.horizontal,
            controller: _controller,
            onPageChanged: onPageChanged,
            children: children,
          ),
        ))
      ],
    );
  }
}
