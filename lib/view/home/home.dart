import 'package:flutter/material.dart';
import 'package:flutter_camera_test_1204/model/mode.dart';
import 'package:flutter_camera_test_1204/view/photo/awsome_photo.dart';
import 'package:flutter_camera_test_1204/widgets/navigation_bottom.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

import '../../provider/home_provider.dart';

class HomeScreen extends HookConsumerWidget {
  HomeScreen({super.key});

  GlobalKey<NavigatorState> _yourKey = GlobalKey<NavigatorState>();
  final homeProvider =
      StateNotifierProvider<HomeProvider, HomeModel>((ref) => HomeProvider());

  static Route<void> route() {
    return MaterialPageRoute<void>(builder: (_) => HomeScreen());
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final home = ref.watch(homeProvider);

    return Scaffold(
      key: _yourKey,
      extendBody: true,
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      resizeToAvoidBottomInset: false,
      body: WillPopScope(
          child: _pageByIndex(home.selectedIndex, context),
          onWillPop: _onWillPop),
      bottomNavigationBar: FABBottomAppBar(
        height: MediaQuery.of(context).size.height * 0.1,
        onTabSelected: (index) {
          ref.watch(homeProvider.notifier).changedIndex(index);
        },
        initIndex: home.selectedIndex,
        backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
        items: [
          FABBottomAppBarItem(
            assetImage: home.selectedIndex != 0
                ? 'assets/images/home.png'
                : 'assets/images/selected_home.png',
          ),
          FABBottomAppBarItem(
            assetImage: home.selectedIndex != 1
                ? 'assets/images/foot.png'
                : 'assets/images/selected_foot.png',
          ),
          FABBottomAppBarItem(
            assetImage: home.selectedIndex != 2
                ? 'assets/images/map_bottom.png'
                : 'assets/images/selected_map_bottom.png',
          ),
          FABBottomAppBarItem(
            assetImage: home.selectedIndex != 3
                ? 'assets/images/mypage.png'
                : 'assets/images/selected_mypage.png',
          ),
        ],
      ),
    );
  }

  Widget _pageByIndex(int index, BuildContext context) {
    switch (index) {
      case 0:
        return AwesomeCameraPage(context); // 메인화면
      case 1:
        return Container();
      case 2:
        return Container();
      case 3:
        return Container();
      default:
        return Container();
    }
  }

  Future<bool> _onWillPop() async {
    //Checks if current Navigator still has screens on the stack.
    if (_yourKey.currentState!.canPop()) {
      // 'maybePop' method handles the decision of 'pop' to another WillPopScope if they exist.
      //If no other WillPopScope exists, it returns true
      _yourKey.currentState?.maybePop();
      return Future<bool>.value(false);
    }
//if nothing remains in the stack, it simply pops
    return Future<bool>.value(true);
  }
}
