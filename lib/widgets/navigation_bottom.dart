import 'package:flutter/material.dart';

class FABBottomAppBarItem {
  FABBottomAppBarItem({this.assetImage, this.text, this.notiCnt});
  String? assetImage;
  String? text;
  int? notiCnt;
}

class FABBottomAppBar extends StatefulWidget {
  FABBottomAppBar({
    super.key,
    this.items,
    this.centerItemText,
    this.height = 60.0,
    this.iconSize = 24.0,
    this.backgroundColor,
    this.notchedShape,
    this.onTabSelected,
    required this.initIndex,
  }) {
    assert(items?.length == 3 || items?.length == 4);
  }
  final List<FABBottomAppBarItem>? items;
  final String? centerItemText;
  final double height;
  final double iconSize;
  final Color? backgroundColor;
  final NotchedShape? notchedShape;
  final ValueChanged<int>? onTabSelected;
  final int initIndex;

  @override
  State<StatefulWidget> createState() => FABBottomAppBarState();
}

class FABBottomAppBarState extends State<FABBottomAppBar> {
  _updateIndex(int index) {
    if (widget.onTabSelected != null) {
      widget.onTabSelected!(index);
    }
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> items = List.generate(widget.items!.length, (int index) {
      return _buildTabItem(
        widget.items![index],
        index: index,
        onPressed: _updateIndex,
      );
    });

    return BottomAppBar(
      color: widget.backgroundColor,
      height: widget.height,
      padding: EdgeInsets.zero,
      child: Container(
        decoration: const BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(255, 255, 255, 0.2),
              spreadRadius: 0,
              blurRadius: 5,
              offset: Offset(0, -5), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: items,
        ),
      ),
    );
  }

  // Widget _buildMiddleTabItem() {
  //   return Expanded(
  //     child: SizedBox(
  //       height: 0,
  //       child: Column(
  //         mainAxisSize: MainAxisSize.min,
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         children: <Widget>[
  //           SizedBox(height: widget.iconSize),
  //           Text(
  //             widget.centerItemText ?? '',
  //             style: TextStyle(color: widget.color),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }

  Widget _buildTabItem(
    FABBottomAppBarItem item, {
    int? index,
    ValueChanged<int>? onPressed,
  }) {
    return Expanded(
      child: SizedBox(
        height: widget.height,
        child: Material(
          type: MaterialType.canvas,
          child: InkWell(
            onTap: () => onPressed != null ? onPressed(index!) : null,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // item!.text == '채팅' && item.notiCnt! > 0
                //     ? Badge(
                //         badgeContent: Text(
                //           '${item.notiCnt}',
                //           style: TextStyle(color: Colors.white),
                //         ),
                //         child: Icon(item!.iconData,
                //             color: color, size: widget.iconSize),
                //       )
                Image.asset(
                  item.assetImage ?? '',
                  width: 28,
                  height: 28,
                  fit: BoxFit.fill,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
