import 'package:flutter/material.dart';
import 'package:flutter_camera_test_1204/app.dart';
import 'package:flutter_camera_test_1204/repository/authentication_repository.dart';
import 'package:flutter_camera_test_1204/repository/user_repository.dart';
import 'package:hooks_riverpod/hooks_riverpod.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final authRepository = AuthenticationRepository();
  await authRepository.logIn(username: 'admin', password: 'admin123');
  final userRepository = UserRepository();
  runApp(ProviderScope(
      child: App(
    authenticationRepository: authRepository,
    userRepository: userRepository,
  )));
}
