import 'package:flutter_camera_test_1204/model/mode.dart';
import 'package:uuid/uuid.dart';

class UserRepository {
  User? _user;

  Future<User?> getUser() async {
    if (_user != null) return _user;
    return Future.delayed(
        Duration(milliseconds: 300), () => _user = User(token: Uuid().v4()));
  }
}
