import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'photo_event.dart';
part 'photo_state.dart';

class CameraBloc extends Bloc<CameraEvent, CameraState> {
  CameraBloc() : super(const CameraState.initialize()) {
    on<PhotoEvent>(_onTakePicture);
    on<ZoomEvent>(_onZoom);
    on<OnTapMediaCaptureEvent>(_onTapMediaCapture);
    on<onBackButtonEvent>(_onBackbutton);
  }

  Future<void> _onTakePicture(
      PhotoEvent event, Emitter<CameraState> emit) async {
    emit(const CameraState.takeaphoto());
  }

  Future<void> _onZoom(ZoomEvent event, Emitter<CameraState> emit) async {}

  Future<void> _onTapMediaCapture(
      OnTapMediaCaptureEvent event, Emitter<CameraState> emit) async {
    print(event.filePath);
    if (event.filePath.isNotEmpty) {
      print('#1');
      emit(CameraState.onTapMediaCapture(event.filePath));
    } else {
      print('#2');
      emit(const CameraState.failure());
    }
  }

  Future<void> _onBackbutton(
      onBackButtonEvent event, Emitter<CameraState> emit) async {
    emit(const CameraState.pop());
  }
}
