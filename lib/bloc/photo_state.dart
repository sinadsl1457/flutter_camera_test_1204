part of 'photo_bloc.dart';

enum CameraStatus {
  takephoto,
  takevideo,
  zoomin,
  zoomout,
  failure,
  initialize,
  ontapMediaCapture,
  pop,
}

class CameraState extends Equatable {
  const CameraState._({
    required this.status,
    this.filepath = '',
  });

  const CameraState.initialize() : this._(status: CameraStatus.initialize);
  const CameraState.takeaphoto() : this._(status: CameraStatus.takephoto);
  const CameraState.takeavideo() : this._(status: CameraStatus.takevideo);
  const CameraState.zoomin() : this._(status: CameraStatus.zoomin);
  const CameraState.zoomout() : this._(status: CameraStatus.zoomout);
  const CameraState.failure() : this._(status: CameraStatus.failure);
  const CameraState.pop() : this._(status: CameraStatus.pop);
  const CameraState.onTapMediaCapture(String filepath)
      : this._(status: CameraStatus.ontapMediaCapture, filepath: filepath);

  final CameraStatus status;
  final String filepath;

  @override
  List<Object> get props => [
        status,
        filepath,
      ];
}
