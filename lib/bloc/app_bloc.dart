import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_camera_test_1204/model/mode.dart';
import 'package:flutter_camera_test_1204/repository/authentication_repository.dart';
import 'package:flutter_camera_test_1204/repository/user_repository.dart';

part 'app_event.dart';
part 'app_state.dart';

class AppBloc extends Bloc<AppEvent, AppState> {
  final AuthenticationRepository _authenticationRepository;
  final UserRepository _userRepository;
  late final StreamSubscription<AppStatus> _userSubscription;

  AppBloc(
      {required AuthenticationRepository authenticationRepository,
      required UserRepository userRepository})
      : _authenticationRepository = authenticationRepository,
        _userRepository = userRepository,
        super(const AppState.unknown()) {
    on<AppUserChanged>(_onAppUserChanged);
    _userSubscription = _authenticationRepository.status.listen((status) {
      add(AppUserChanged(status));
    });
  }

  Future<void> _onAppUserChanged(
      AppUserChanged event, Emitter<AppState> emit) async {
    switch (event.status) {
      case AppStatus.unauthenticated:
        return emit(const AppState.unauthenticated());
      case AppStatus.authenticated:
        final user = await _tryGetUser();
        return emit(
          user != null
              ? AppState.authenticated(user)
              : const AppState.unauthenticated(),
        );
      case AppStatus.unknown:
        return emit(const AppState.unknown());
    }
  }

  @override
  Future<void> close() {
    _userSubscription.cancel();
    return super.close();
  }

  Future<User?> _tryGetUser() async {
    try {
      final user = await _userRepository.getUser();
      return user;
    } catch (_) {
      return null;
    }
  }
}
