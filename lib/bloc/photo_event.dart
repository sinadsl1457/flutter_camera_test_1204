part of 'photo_bloc.dart';

sealed class CameraEvent extends Equatable {
  const CameraEvent();

  @override
  List<Object> get props => [];
}

final class PhotoEvent extends CameraEvent {
  const PhotoEvent();
}

final class ZoomEvent extends CameraEvent {
  const ZoomEvent();
}

final class OnTapMediaCaptureEvent extends CameraEvent {
  const OnTapMediaCaptureEvent(
    this.filePath,
  );

  final String filePath;
}

final class onBackButtonEvent extends CameraEvent {
  const onBackButtonEvent();
}
