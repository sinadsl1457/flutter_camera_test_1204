import 'package:flutter_riverpod/flutter_riverpod.dart';

class HomeModel {
  int selectedIndex;

  HomeModel({
    required this.selectedIndex,
  });
}

class HomeProvider extends StateNotifier<HomeModel> {
  HomeProvider() : super(HomeModel(selectedIndex: 0));

  void changedIndex(int index) {
    state = HomeModel(selectedIndex: index);
  }
}
